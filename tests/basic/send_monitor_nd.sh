#!/bin/sh

ret=0
nsuccess=0
nfail=0
NETNS_0="ns-$(mktemp -u XXXXXX)"
NETNS_1="ns-$(mktemp -u XXXXXX)"

# $1: return value
# $2: log message
libndp_log() {
	local rc=$1
	local msg="$2"

	if [ $rc -eq 0 ]; then
		printf "[libndp gating]: %-30s [ PASS ]\n" "$msg"
		nsuccess=$((nsuccess+1))
	else
		printf "[libndp gating]: %-30s [ FAIL ]\n" "$msg"
		nfail=$((nfail+1)) && ret=1
	fi
}

install_tcpreplay() {
	which tcpreplay && return 0
	local ver="4.3.4"
	wget https://github.com/appneta/tcpreplay/releases/download/v${ver}/tcpreplay-${ver}.tar.xz
	tar xf tcpreplay-${ver}.tar.xz
	cd tcpreplay-${ver}
	./configure && make && make install
	cd ../
	which tcpreplay
}

clean_up() {
	ip netns del ${NETNS_0} &> /dev/null
	ip netns del ${NETNS_1} &> /dev/null
	ip link del veth0 &> /dev/null
	ip link del veth1 &> /dev/null
}

trap clean_up EXIT

setup() {
	install_tcpreplay &> tcpreplay.log || \
		{ libndp_log $? "Install tcpreplay" && cat tcpreplay.log; return 1; }

	clean_up

	ip netns add "${NETNS_0}"
	ip netns add "${NETNS_1}"
	ip -netns "${NETNS_0}" link set lo up
	ip -netns "${NETNS_1}" link set lo up

	ip link add type veth
	ip link set veth0 netns "${NETNS_0}"
	ip link set veth1 netns "${NETNS_1}"
	ip -netns "${NETNS_0}" link set veth0 addr 00:00:00:00:00:01
	ip -netns "${NETNS_1}" link set veth1 addr 00:00:00:00:00:02
	ip -netns "${NETNS_0}" link set veth0 up
	ip -netns "${NETNS_1}" link set veth1 up
}

monitor_nd()
{
	ip netns exec "${NETNS_1}" unbuffer ndptool -i veth1 monitor &> monitor.log &
	sleep 2
	# TODO: maybe fuzz the pcap file and see if ndptool still works
	ip netns exec "${NETNS_0}" tcpreplay -l 2 -i veth0 nd.pcap
	sleep 2 && kill -9 $!

	grep -q "Type: NS" monitor.log; libndp_log $? "Monitor NS"
	grep -q "Type: NA" monitor.log; libndp_log $? "Monitor NA"
	# commit this due to bug 1082933
	# grep -q "Type: RS" monitor.log; libndp_log $? "Monitor RS"
	grep -q "Type: RA" monitor.log; libndp_log $? "Monitor RA"
	# TODO: add ICMPv6 redirect support

}

send_nd()
{
	ip netns exec "${NETNS_1}" tcpdump -i veth1 -nn -l &> rs.log &
	sleep 2
	ip netns exec "${NETNS_0}" ndptool -i veth0 -t rs send
	sleep 2 && kill -9 $!

	#ip netns exec "${NETNS_0}" ndptool -i veth0 -t ra send

	ip netns exec "${NETNS_1}" tcpdump -i veth1 -nn -l &> ns.log &
	sleep 2
	ip netns exec "${NETNS_0}" ndptool -i veth0 -t ns send
	sleep 2 && kill -9 $!

	ip netns exec "${NETNS_1}" tcpdump -i veth1 -nn -l &> ns_t.log &
	sleep 2
	ip netns exec "${NETNS_0}" ndptool -i veth0 -t ns -T fe80::200:ff:fe00:2 send
	sleep 2 && kill -9 $!

	#ip netns exec "${NETNS_0}" ndptool -i veth0 -t na send

	ip netns exec "${NETNS_1}" tcpdump -i veth1 -nn -l &> na_d_t.log &
	sleep 2
	ip netns exec "${NETNS_0}" ndptool -i veth0 -t na -D fe80::200:ff:fe00:2 -T fe80::200:ff:fe00:1 send
	sleep 2 && kill -9 $!

	ip netns exec "${NETNS_1}" tcpdump -i veth1 -nn -l &> na_u.log &
	sleep 2
	ip netns exec "${NETNS_0}" ndptool -i veth0 -U -t na send
	sleep 2 && kill -9 $!

	grep -q "fe80::200:ff:fe00:1 > ff02::2: ICMP6, router solicitation" rs.log; libndp_log $? "Send RS"
	grep -q "fe80::200:ff:fe00:1 > ff02::1: ICMP6, neighbor solicitation, who has ::" ns.log; libndp_log $? "Send NS"
	grep -q "fe80::200:ff:fe00:1 > ff02::1:ff00:2: ICMP6, neighbor solicitation, who has fe80::200:ff:fe00:2" ns_t.log; libndp_log $? "Send NS with target"
	grep -q "fe80::200:ff:fe00:1 > fe80::200:ff:fe00:2: ICMP6, neighbor advertisement, tgt is fe80::200:ff:fe00:1" na_d_t.log; libndp_log $? "Send NA with dest and target"
	grep -q "fe80::200:ff:fe00:1 > ff02::1: ICMP6, neighbor advertisement, tgt is ::" na_u.log; libndp_log $? "Send unsolicited NA"
}

setup || exit 1
monitor_nd
send_nd

printf "\nTests passed: %3d\n" ${nsuccess}
printf "Tests failed: %3d\n"   ${nfail}

if [ ${nfail} -ne 0 ]; then
	echo -e "\nDEBUG INFO:\n"
	for log in `ls *.log`; do
		echo "# cat $log"
		cat $log
	done
fi

exit $ret
